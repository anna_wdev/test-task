var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();

var cssSources = [
	'./node_modules/normalize.css/normalize.css',
	'./src/styles/main.scss'
];

gulp.task('default', ['sass', 'js']);
gulp.task('production', ['sass:production', 'js']);

gulp.task('sass:production', function() {
	return gulp.src(cssSources)
		.pipe(sass().on('error', sass.logError))
		.pipe(concat('main.css'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('sass', function() {
	return gulp.src(cssSources)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(sourcemaps.write('./maps'))
		.pipe(concat('main.css'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('js', function() {
	return gulp.src([
		'./node_modules/jquery/dist/jquery.js',
		'./src/scripts/**/*.js'
		])
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('./dist'))
		.pipe(browserSync.stream());
});

gulp.task('watch:sass', function() {
	gulp.watch('./src/styles/**/*.scss', ['sass']);
});

gulp.task('watch:js', function() {
	gulp.watch('./src/scripts/**/*.js', ['js']);
});

gulp.task('server', ['sass', 'js'], function() {
	// Serve files from the root of this project
	browserSync.init({
		server: {
			baseDir: './'
		}
	});

	gulp.watch('./src/styles/**/*.scss', ['sass']);
	gulp.watch('./src/scripts/**/*.js', ['js']);
	gulp.watch('./**/*.html').on('change', browserSync.reload);
});