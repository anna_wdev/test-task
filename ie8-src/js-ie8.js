$(document).ready(function() {
	$('.grid-items ul li:nth-child(3n+3)').addClass('nth-child-3np1');

	initCheckedStarHandler();
	initCheckedSizeHandler();

	$('.select-size input.size').on('click', function() {
		$('input.size').siblings('label').removeClass('checked');
		$('input.size:checked').siblings('label').addClass('checked');
	});

	$('label.star').on('click', function() {
		var forId = $(this).attr('for');
		var $el = $('input#' + forId);

		$('input.star').removeClass('checked');
		$el.addClass('checked');
	});

	function initCheckedStarHandler() {
		$('.stars-rating input.star').each(function(index, item) {
			var isChecked = this.checked;

			if (isChecked) {
				$(item).addClass('checked');
			}
		});
	};

	function initCheckedSizeHandler() {
		$('.select-size input.size').each(function(index, item) {
			var isChecked = this.checked;

			if (isChecked) {
				$(item).siblings('label').addClass('checked');
			}
		});
	};
});
