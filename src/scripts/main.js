/**
 * Share pop-up
 * @module
 */
var SharePopUp = function() {
	var defaults = {
		selectors: {
			shareLink: '.social-share-title',
			shareContainer: '.social-share-container',
			shareCloseButton: '.social-share-close-btn'
		},
		classNames: {
			hiddenMd: 'hidden-md'
		}
	};
	var settings;

	/**
	 * Toggle share pop-up
	 * @param {Object} [options]
	 * @public
	 */
	function init(options) {
		options = options || {};
		settings = $.extend(true, {}, defaults, options);

		attachEvents();
	}

	/**
	 * Function attaches events
	 */
	function attachEvents() {
		$(settings.selectors.shareLink).on('click touchend', function() {
			$(settings.selectors.shareContainer).removeClass(settings.classNames.hiddenMd);
		});

		$(settings.selectors.shareCloseButton).on('click touchend', function() {
			$(settings.selectors.shareContainer).addClass(settings.classNames.hiddenMd);
		});
	}


	/**
	 * @exports
	 */
	return {
		init: init
	};
};

/**
 * Custom Slider
 * @returns {*|Array|Object}
 */
$.fn.gamersSlider = function(options) {
	options = options || {};

	var defaults = {
		selectors: {
			sliderItems: '.js-slider-items',
			sliderItem: '.item',
			controlPrev: '.js-arrow-prev',
			controlNext: '.js-arrow-next'
		},
		classNames: {
			hiddenXs: 'hidden-xs',
			slidePrev: 'slide-prev',
			slideNext: 'slide-next'
		},
		animationTimeout: 400
	};
	var settings = $.extend(true, {}, defaults, options);
	var self = this;

	$(settings.selectors.controlNext).on('click', function() {
		var $sliderVisibleItem = self.find(settings.selectors.sliderItem + ':not(.hidden-xs)');
		var isLast = $sliderVisibleItem.is(':last-child');

		if (isLast) {
			return;
		}

		$sliderVisibleItem
			.addClass(settings.classNames.slideNext)
			.delay(settings.animationTimeout)
			.queue(function() {
				$sliderVisibleItem
					.addClass(settings.classNames.hiddenXs)
					.removeClass(settings.classNames.slideNext)
					.next(settings.selectors.sliderItem)
					.removeClass(settings.classNames.hiddenXs)
					.dequeue();
			});
	});

	$(settings.selectors.controlPrev).on('click', function() {
		var $sliderVisibleItem = self.find(settings.selectors.sliderItem + ':not(.hidden-xs)');
		var isFirst = $sliderVisibleItem.is(':first-child');

		if (isFirst) {
			return;
		}

		$sliderVisibleItem
			.addClass(settings.classNames.slidePrev)
			.delay(settings.animationTimeout)
			.queue(function() {
				$sliderVisibleItem
					.addClass(settings.classNames.hiddenXs)
					.removeClass(settings.classNames.slidePrev)
					.prev(settings.selectors.sliderItem)
					.removeClass(settings.classNames.hiddenXs)
					.dequeue();
			});
	});

	return this;
};

